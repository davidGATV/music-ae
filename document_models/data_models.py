from dotmap import DotMap


class AudioBaseDocument:
    def dict_from_class(self):
        return dict((key, value)
                    for (key, value) in self.__dict__.items())

    @staticmethod
    def dot_map_from_dict(data_dict: {dict}):
        return DotMap(data_dict)


class AudioFeatureDocument(AudioBaseDocument):
    def __init__(self, mel: list, chroma: list, mel_size: tuple, chroma_size: tuple,
                 total_segments: int, segment_id: str, track_id: str):
        self.mel: list = mel
        self.chroma: list = chroma
        self.mel_size: tuple = mel_size
        self.chroma_size: tuple = chroma_size
        self.total_segments: int = total_segments
        self.segment_id: str = segment_id
        self.track_id: str = track_id


class EmbeddingDocument(AudioBaseDocument):
    def __init__(self, data: list, track_id: str, segment_id: int, n_segments: int,
                 mel_embedding_size: int, chroma_embedding_size: int):
        self.data: list = data
        self.track_id: str = track_id
        self.segment_id: int = segment_id
        self.n_segments: int = n_segments
        self.mel_embedding_size: int = mel_embedding_size
        self.chroma_embedding_size: int = chroma_embedding_size