import os, sys

import numpy as np
if "test" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."
else:
    os.environ["GLOBAL_PARENT_DIR"] = ""

from managers.data_manager import DataManager
from managers.model_manager import ModelManager
from managers.audio_feature_manager import AudioFeatureManager
from keras.models import Model
from helper import global_variables as gv
from sklearn.metrics.pairwise import cosine_similarity
from search_algorithms.local_sensing_hashing import LSH

data_manager: DataManager = DataManager()
data_manager.init_mongo_connection()
# Retrieve inputs shapes
mel_dim, chroma_dim = data_manager.extract_data_input_shape_from_doc()

# Initialise model object
model_manager = ModelManager(mel_dim=mel_dim, chroma_dim=chroma_dim)

model_directory: str = "trained_models"
model_name: str = gv.model_name + ".h5"
history_directory: str = model_directory
history_name: str = model_name + "_history.json"

model: Model = model_manager.load_trained_model(model_directory=model_directory,
                                                model_name=model_name)
encoder_mel: str = "encoder_mel"
encoder_chroma: str = "encoder_chroma"
compressed_model: Model = model_manager.load_encoder_model(model_directory=model_directory,
                                                           model_name=model_name,
                                                           encoder_mel=encoder_mel,
                                                           encoder_chroma=encoder_chroma)


# Load input
audio_filename: str = "beyond_the_waterfall.mp3"
audio_feat_manager: AudioFeatureManager = AudioFeatureManager(parent_path=audio_filename)
features: dict = audio_feat_manager.retrieve_audio_features(audio_dir="", audio_filename=audio_filename,
                                                            window=True, seconds_per_analysis=gv.seconds_per_analysis)
mel_data = list(features["mel"])
chroma_data = list(features["chroma"])
indexes = [i for i in range(len(mel_data))]
mel_dim = (1,) + mel_dim + (1,)
chroma_dim = (1,) + chroma_dim + (1,)

x_all_embedding: np.array = np.array([])
for index, mel, chroma in zip(indexes, mel_data, chroma_data):
    x_mel = np.array(mel).reshape(mel_dim)
    x_chroma = np.array(chroma).reshape(chroma_dim)

    x_embedding: list = compressed_model.predict([x_mel, x_chroma])
    # Concatenate all embeddings
    x_embedding_np: np.array = np.concatenate([i for i in x_embedding], axis=1)
    if index == 0:
        x_all_embedding = x_embedding_np
    else:
        # Concatenate
        x_all_embedding = np.concatenate([x_all_embedding, x_embedding_np], axis=0)

print(x_all_embedding.shape)


# test LSH
data: np.ndarray = x_all_embedding
num_tables: int = 64
hash_size: int = 6
input_idx: int = 4

lsh = LSH(num_tables=num_tables,
          hash_size=hash_size,
          inp_dimensions=data.shape[1])

for i in range(data.shape[0]):
    vec = data[i, :].flatten()
    lsh.__setitem__(inp_vec=vec, label=str(i))

query_vec = data[input_idx, :].flatten()
res = lsh.__getitem__(inp_vec=query_vec)
print(res)

idx = int(res[0])
print(cosine_similarity(data[idx, :].reshape((1, -1)),
                        query_vec.reshape(1, -1)))