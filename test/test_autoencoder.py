import os, sys
if "test" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."
else:
    os.environ["GLOBAL_PARENT_DIR"] = ""
    
from managers.data_manager import DataManager
from keras.callbacks import History
from helper import global_variables as gv


data_manager: DataManager = DataManager()

data_manager.init_mongo_connection()

model_directory: str = "trained_models"
model_name: str = gv.model_name
history_directory: str = model_directory
history_name: str = model_name + "_history.json"
autoencoder_hs: History = data_manager.train_music_autoencoder(model_directory=model_directory,
                                                               model_name=model_name,
                                                               history_directory=history_directory,
                                                               history_name=history_name)