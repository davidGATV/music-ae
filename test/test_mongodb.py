from helper import global_variables as gv
from managers.mongodb_manager import MongoDBManager


collection_name: str = gv.audio_collection_name
db_name = gv.audio_db_name
# gv.audio_db_name
field_name = "track_id"
value = "4dVuxaj9278T0Md4UUtkUY"
mongodb_manager: MongoDBManager = MongoDBManager(host=gv.db_host, port=gv.db_port,
                                                 username=gv.db_username,
                                                 password=gv.db_password,
                                                 db_name=db_name)
mongodb_manager.set_up_db()

filter = {"track_id": value, "segment_id": 1}
updated_doc = {"track_id": "123456"}
doc = mongodb_manager.find_and_replace_document(collection_name=collection_name, filter=filter,
                                                updated_doc=updated_doc)

mongodb_manager.remove_all_documents_from_collection(collection_name=collection_name)


mycol = mongodb_manager.db[gv.audio_collection_name]

mycol.drop()