import librosa
import librosa.display
import numpy as np

audio_file = "track_00ElYia9fR4f5mKVq7DGOW.mp3"
y, sr = librosa.load(audio_file, sr = None)
S = librosa.feature.melspectrogram(y=y, sr=sr)
S_db = librosa.core.power_to_db(S)
S_pcen = librosa.core.pcen(S)

print(np.min(S_db))
print(np.max(S_db))
print(np.mean(S_db))
print(np.std(S_db))

print(np.min(S_pcen))
print(np.max(S_pcen))
print(np.mean(S_pcen))
print(np.std(S_pcen))
