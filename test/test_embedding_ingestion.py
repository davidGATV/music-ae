import os, sys
if "test" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."
else:
    os.environ["GLOBAL_PARENT_DIR"] = ""

from managers.data_manager import DataManager
from helper import global_variables as gv


data_manager: DataManager = DataManager()
data_manager.init_mongo_connection()

data_manager.mongo_manager.remove_all_documents_from_collection(collection_name=gv.embedding_collection_name)

model_directory: str = os.path.join(os.getenv("GLOBAL_PARENT_DIR"),
                                    "trained_models")
model_name: str = gv.model_name + ".h5"
encoder_mel: str = "encoder_mel"
encoder_chroma: str = "encoder_chroma"
force_features: bool = True

data_manager.ingest_embeddings_into_database(model_directory=model_directory, model_name=model_name,
                                             encoder_mel=encoder_mel, encoder_chroma=encoder_chroma,
                                             force_features=force_features)