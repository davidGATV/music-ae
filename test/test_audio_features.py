import os

from helper import global_variables as gv
from analysis.audio_analysis import AudioAnalysis
from helper.utils import generate_uuid_from_string
from managers.serializable_iterator import SerializableGenerator


def retrieve_audio_features(audio_dir: str, audio_filename: str, window: bool = True,
                            seconds_per_analysis: int = 6):
    features: dict = {}
    try:
        # Extract features from audio signal
        audio_analysis_man: AudioAnalysis = AudioAnalysis(audio_dir=audio_dir, audio_filename=audio_filename)
        features: dict = audio_analysis_man.execute_analysis(window=window, seconds_per_analysis=seconds_per_analysis)
    except Exception as e:
        gv.logger.error(e)
    return features


def build_documents(input_document: dict, uuid_param: str):
    output_document: SerializableGenerator = SerializableGenerator([])
    try:
        # Generate UUID
        track_uuid: str = generate_uuid_from_string(data_uuid=[uuid_param])
        output = [{'mel': mel, 'chroma': chroma, 'mel_size': input_document["mel_size"],
                   'chroma_size': input_document["chroma_size"],
                   'total_segments': input_document["n_segments"],
                   'segment_id': i,
                   'track_id': track_uuid} for i, mel, chroma in zip(list(range(1, input_document["n_segments"]+1)),
                                                                     list(input_document["mel"]),
                                                                     list(input_document["chroma"]))]
        # Generate documents
        output_document = SerializableGenerator(output)
    except Exception as e:
        gv.logger.error(e)
    return output_document


if __name__ == '__main__':
    if "test" in os.getcwd():
        os.environ["GLOBAL_PARENT_DIR"] = ".."
    else:
        os.environ["GLOBAL_PARENT_DIR"] = ""

    seconds_per_analysis: int = 10
    window: bool = True
    audio_dir: str = "Dataset_test"
    audio_filename: str = "track_0hnqrVLtwbXTAFtE4XpCds.mp3"
    features: dict = retrieve_audio_features(audio_dir=audio_dir, audio_filename=audio_filename,
                                             window=window, seconds_per_analysis=seconds_per_analysis)

    output_doc: SerializableGenerator = build_documents(input_document=features, uuid_param=audio_filename)
    for i, m in enumerate(output_doc):
        print(type(m["mel"]))
        print(i)

