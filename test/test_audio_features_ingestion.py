import os, sys
import pandas as pd
print(os.getcwd())
if "test" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."
else:
    os.environ["GLOBAL_PARENT_DIR"] = ""


from helper import global_variables as gv
from managers.data_manager import DataManager


if __name__ == '__main__':

    data_path: str = os.path.join(gv.global_parent_dir, "resources", "spotify_tracks.csv")

    df: pd.DataFrame = pd.read_csv(data_path, index_col=0)
    data_set_path = "D:\\DAVID\\Datasets"
    parent_path: str = os.path.join(data_set_path, "SpotTrack101")
    uuid_col: str = "spotify_id"
    file_extension: str = gv.file_extension
    data_manager: DataManager = DataManager()
    data_manager.generate_audio_features_from_dataframe(parent_path=parent_path,
                                                        file_extension=file_extension,
                                                        df=df, uuid_col=uuid_col)
