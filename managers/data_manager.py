import os
import pandas as pd
import numpy as np
from managers.audio_feature_manager import AudioFeatureManager
from managers.mongodb_manager import MongoDBManager
from managers.model_manager import ModelManager
from helper import global_variables as gv
from helper.utils import prepare_directory
from managers.serializable_iterator import SerializableGenerator
from document_models.data_models import EmbeddingDocument
from pymongo.cursor import Cursor
from typing import Optional
from keras.models import Model
from keras.callbacks import History


class DataManager:
    def __init__(self):
        self.audio_feature_manager: Optional[AudioFeatureManager] = None
        self.seconds_per_analysis: int = gv.seconds_per_analysis
        self.mongo_manager: MongoDBManager = MongoDBManager(host=gv.db_host, port=gv.db_port,
                                                            username=gv.db_username, password=gv.db_password,
                                                            db_name=gv.audio_db_name)

        self.model_manager: Optional[ModelManager] = None
        self.audio_collection_name: str = gv.audio_collection_name
        self.embedding_collection_name: str = gv.embedding_collection_name
    
    def init_audio_feature_manager(self, parent_path: str, file_extension: str):
        try:
            self.audio_feature_manager: AudioFeatureManager = AudioFeatureManager(
                parent_path=parent_path,
                file_extension=file_extension)
        except Exception as e:
            gv.logger.error(e)
        return
            
    def init_mongo_connection(self):
        try:
            self.mongo_manager.set_up_db()
        except Exception as e:
            gv.logger.error(e)

    def generate_audio_features_from_dataframe(self, parent_path: str,  file_extension: str ,
                                               df: pd.DataFrame, uuid_col: str,
                                               force_features: bool = False):
        try:
            if self.mongo_manager.db is None:
                self.init_mongo_connection()
            if self.audio_feature_manager is None:
                self.init_audio_feature_manager(parent_path=parent_path,
                                                file_extension=file_extension)
                # Prepare directory
                gv.logger.info("Preparing directory at %s", parent_path)
                prepare_directory(parent_path)

                # Start Process
                for index, row in df.iterrows():
                    try:
                        gv.logger.warn("Processing Track %s/%s", int(index) + 1, str(df.shape[0]))
                        preview_url = row["preview_url"]
                        track_id = row[uuid_col]
                        audio_name = 'track' + '_' + track_id + file_extension

                        # Download file
                        # verify if file exists:
                        if not os.path.exists(os.path.join(parent_path, audio_name)):
                            done: bool = self.audio_feature_manager.download_audio_file(preview_url=preview_url,
                                                                                        parent_path=parent_path,
                                                                                        audio_name=audio_name)
                        else:
                            done: bool = True if force_features else False

                        if done:
                            # 1) Extract Features
                            data_features: dict = self.audio_feature_manager.retrieve_audio_features(
                                audio_dir=parent_path,
                                audio_filename=audio_name,
                                seconds_per_analysis=self.seconds_per_analysis)

                            # 2) Build document
                            output_doc: SerializableGenerator = self.audio_feature_manager.build_documents(
                                input_document=data_features, uuid_param=track_id)

                            # 3) Check if the document does not exist
                            filter_data: dict = {"track_id": track_id}
                            res_doc: Cursor = self.mongo_manager.find_document_by_filter(
                                collection_name=self.audio_collection_name, filter=filter_data)
                            not_exist: bool = False if len(list(res_doc)) else True
                            if not_exist:
                                gv.logger.info("Ingesting document at %s", self.audio_collection_name)
                                for i, doc in enumerate(output_doc):
                                    self.mongo_manager.insert_document_to_collection(
                                        collection_name=self.audio_collection_name,
                                        document=doc)
                            else:
                                # If force features
                                if force_features:
                                    for i, doc in enumerate(output_doc):
                                        # Find and update
                                        filter_data: dict = {"track_id": track_id,
                                                             "segment_id": str(i+1)}
                                        self.mongo_manager.find_and_replace_document(
                                            collection_name=self.audio_collection_name,
                                            filter=filter_data,
                                            updated_doc=doc)
                    except Exception as e:
                        gv.logger.warning(e)
                        continue
        except Exception as e:
            gv.logger.error(e)

    def retrieve_all_documents_ids(self):
        all_docs_ids: iter = iter([])
        try:
            all_docs_ids: iter = self.mongo_manager.find_all_document_ids(
                collection_name=gv.audio_collection_name)
        except Exception as e:
            gv.logger.error(e)
        return all_docs_ids

    def extract_data_input_shape_from_doc(self):
        mel_size: tuple = ()
        chroma_size: tuple = ()
        try:
            # Retrieve sampling doc to get the mel_size and chroma size
            sampling_doc: dict = self.mongo_manager.find_one_document(
                collection_name=self.audio_collection_name)
            mel_size: tuple = tuple(sampling_doc["mel_size"])
            chroma_size: tuple = tuple(sampling_doc["chroma_size"])
        except Exception as e:
            gv.logger.error(e)
        return mel_size, chroma_size

    def train_music_autoencoder(self, model_directory: str, model_name: str, history_directory: str,
                                history_name: str):
        autoencoder_history: Optional[History] = None
        try:
            # Retrieve documents
            all_docs_ids: iter = self.retrieve_all_documents_ids()
            # Retrieve inputs shapes
            mel_dim, chroma_dim = self.extract_data_input_shape_from_doc()

            # Initialise model object
            self.model_manager = ModelManager(mel_dim=mel_dim, chroma_dim=chroma_dim)

            # Generate sets
            train_ids, dev_ids, test_ids = self.model_manager.generate_train_dev_test_sets(
                all_docs_ids=list(all_docs_ids))

            # Build autoencoder
            autoencoder: Model = self.model_manager.build_music_autoencoder()

            # Compile model
            autoencoder: Model = self.model_manager.compile_autoencoder(autoencoder=autoencoder)

            # Train autoencoder
            autoencoder_history: History = self.model_manager.train_autoencoder_generator(
                autoencoder=autoencoder, mongo_manager=self.mongo_manager,
                collection_name=self.audio_collection_name, train_ids=train_ids,
                dev_ids=dev_ids)

            # Save data
            self.save_model_information(model=autoencoder, model_history=autoencoder_history,
                                        model_directory=model_directory, model_name=model_name,
                                        history_directory=history_directory, history_name=history_name)
        except Exception as e:
            gv.logger.error(e)
        return autoencoder_history

    def save_model_information(self, model: Model, model_history: History, model_directory: str,
                               model_name: str, history_directory: str, history_name: str):
        try:
            # Model
            self.model_manager.save_trained_model(model=model, model_directory=model_directory,
                                                  model_name=model_name)
            # Model history
            self.model_manager.save_history(history=model_history,
                                            history_directory=history_directory,
                                            history_name=history_name)
        except Exception as e:
            gv.logger.error(e)

    def ingest_embeddings_into_database(self, model_directory: str, model_name: str,
                                        encoder_mel: str, encoder_chroma: str,
                                        force_features: bool = True):
        try:
            # Retrieve all documents into an iterator
            all_docs_ids: iter = self.retrieve_all_documents_ids()
            # Retrieve inputs shapes
            mel_dim, chroma_dim = self.extract_data_input_shape_from_doc()
            # Initialise model object
            self.model_manager = ModelManager(mel_dim=mel_dim, chroma_dim=chroma_dim)

            # Load Model
            autoencoder: Model = self.model_manager.load_encoder_model(model_directory=model_directory,
                                                                       model_name=model_name,
                                                                       encoder_mel=encoder_mel,
                                                                       encoder_chroma=encoder_chroma)
            # Get encoder sizes
            mel_embedding_size: int = self.model_manager.get_encoder_size_by_name(
                model=autoencoder, layer_name=encoder_mel)
            chroma_embedding_size: int = self.model_manager.get_encoder_size_by_name(
                model=autoencoder, layer_name=encoder_chroma)
            # Adequate input
            mel_dim = (1,) + mel_dim + (1,)
            chroma_dim = (1,) + chroma_dim + (1,)

            # Start process
            for doc_id in all_docs_ids:
                # Get required information
                audio_feature_data: dict = self.mongo_manager.get_document_by_id(
                    collection_name=self.audio_collection_name,
                    uuid=doc_id)
                mel_data: list = list(audio_feature_data["mel"])
                chroma_data: list = list(audio_feature_data["chroma"])
                track_id: str = audio_feature_data["track_id"]
                n_segments: int = audio_feature_data["total_segments"]
                segment_id: int = int(audio_feature_data["segment_id"])

                # Generate embedding
                x_embedding: np.array = self.model_manager.generate_embeddings(
                    model=autoencoder, mel_data=mel_data, chroma_data=chroma_data,
                    mel_dim=mel_dim, chroma_dim=chroma_dim)

                # Check if the document does not exist
                filter_data: dict = {"track_id": track_id}
                res_doc: Cursor = self.mongo_manager.find_document_by_filter(
                    collection_name=self.embedding_collection_name, filter=filter_data)
                not_exist: bool = False if len(list(res_doc)) else True

                if not_exist:
                    gv.logger.info("Ingesting document at %s", self.embedding_collection_name)
                    self.ingest_embedding_document(x_embedding=x_embedding,
                                                   segment_id=segment_id,
                                                   n_segments=n_segments,
                                                   track_id=track_id,
                                                   mel_embedding_size=mel_embedding_size,
                                                   chroma_embedding_size=chroma_embedding_size,
                                                   replace=False)
                else:
                    # If force features
                    if force_features:
                        self.ingest_embedding_document(x_embedding=x_embedding,
                                                       n_segments=n_segments,
                                                       segment_id=segment_id,
                                                       track_id=track_id,
                                                       mel_embedding_size=mel_embedding_size,
                                                       chroma_embedding_size=chroma_embedding_size,
                                                       replace=True)

        except Exception as e:
            gv.logger.error(e)

    def ingest_embedding_document(self, x_embedding: np.array, segment_id: int, track_id: str,
                                  n_segments: int, mel_embedding_size: int, chroma_embedding_size: int,
                                  replace: bool = False):
        try:

            data: list = x_embedding.tolist()
            # Generate embedding document
            embedding_doc: EmbeddingDocument = EmbeddingDocument(
                data=data,
                track_id=track_id,
                segment_id=segment_id,
                n_segments=n_segments,
                mel_embedding_size=mel_embedding_size,
                chroma_embedding_size=chroma_embedding_size)

            # Convert to dict
            doc: dict = embedding_doc.dict_from_class()

            # Find and update
            filter_data: dict = {"track_id": track_id,
                                 "segment_id": segment_id}
            if replace:
                self.mongo_manager.find_and_replace_document(
                    collection_name=self.embedding_collection_name,
                    filter=filter_data,
                    updated_doc=doc)
            else:
                self.mongo_manager.insert_document_to_collection(
                    collection_name=self.embedding_collection_name,
                    document=doc)
        except Exception as e:
            gv.logger.error(e)
