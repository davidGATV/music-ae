import os
import numpy as np
from helper import global_variables as gv
from sklearn.model_selection import train_test_split
from helper.utils import (prepare_directory, write_json_file, read_json_file)
from typing import Optional
from keras import optimizers
from helper.sequence_generator import DataGenerator
from keras.models import Model, load_model
from keras.layers import (Input, Conv2D, Cropping2D, MaxPooling2D,
                          UpSampling2D, ZeroPadding2D,Flatten, Reshape,
                          LeakyReLU)
from managers.mongodb_manager import MongoDBManager
from keras.callbacks import History


class ModelManager:
    def __init__(self, mel_dim: tuple = (), chroma_dim: tuple = ()):
        self.test_size = gv.test_size
        self.dev_size = gv.dev_size
        self.mandatory_shape = gv.mandatory_shape
        self.mel_dim: tuple = mel_dim
        self.chroma_dim: tuple = chroma_dim
        self.input_shape_mel: tuple = mel_dim + (1,)
        self.input_shape_chroma: tuple = chroma_dim + (1,)
        self.encoder_layer_name_mel: str = gv.encoder_layer_name_mel
        self.encoder_layer_name_chroma: str = gv.encoder_layer_name_chroma
        self.last_layer_name_mel: str = gv.last_layer_name_mel
        self.last_layer_name_chroma: str = gv.last_layer_name_chroma
        self.decoder_layer_name_mel: str = gv.decoder_layer_name_mel
        self.decoder_layer_name_chroma: str = gv.decoder_layer_name_chroma
        self.model_name: str = gv.model_name
        self.mandatory_shape: int = gv.mandatory_shape
        self.metrics: list = gv.metrics
        self.losses: list = gv.losses
        self.optimizer_name: str = gv.optimizer_name
        self.batch_size: int = gv.batch_size
        self.epochs: int = gv.epochs
        self.n_channels: int = gv.n_channels

    def generate_train_dev_test_sets(self, all_docs_ids: list):
        train_ids: list = []
        dev_ids: list = []
        test_ids: list = []
        try:
            # Divide document ids into train and test
            train_ids, test_ids = train_test_split(all_docs_ids, test_size=self.test_size)

            train_ids, dev_ids = train_test_split(train_ids, test_size=self.dev_size)
        except Exception as e:
            gv.logger.error(e)
        return train_ids, dev_ids, test_ids

    def build_music_autoencoder(self):
        autoencoder: Optional[Model] = None
        try:
            autoencoder: Model = self.build_autoencoder_system(
                input_shape_mel=self.input_shape_mel,
                input_shape_chroma=self.input_shape_chroma,
                encoder_layer_name_mel=self.encoder_layer_name_mel,
                encoder_layer_name_chroma=self.encoder_layer_name_chroma,
                last_layer_name_mel=self.last_layer_name_mel,
                last_layer_name_chroma=self.last_layer_name_chroma,
                decoder_layer_name_mel=self.decoder_layer_name_mel,
                decoder_layer_name_chroma=self.decoder_layer_name_chroma,
                model_name=self.model_name,
                mandatory_shape=self.mandatory_shape)
        except Exception as e:
            gv.logger.error(e)
        return autoencoder

    @staticmethod
    def build_encoder_branch(inputs, encoder_layer_name: str,
                             mandatory_shape: int,
                             last_layer_name: str):
        encoder: Optional[Flatten] = None
        try:
            # Check input
            if inputs.get_shape()[2] != mandatory_shape:
                crop_val: tuple = (np.abs(int(mandatory_shape - inputs.get_shape()[2])), 0)
            else:
                crop_val: tuple = (0, 0)

            x = Cropping2D(cropping=((0, 0), crop_val), data_format="channels_last")(inputs)
            x = Conv2D(64, (3, 3), padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(16, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same', name=last_layer_name)(x)
            encoder: Flatten = Flatten(name=encoder_layer_name)(x)
        except Exception as e:
            gv.logger.error(e)
        return encoder

    @staticmethod
    def build_decoder_branch(input_encoded: Flatten, decoder_layer_name: str,
                             reshaped_input: tuple,
                             crop_up_sampling: bool = False):
        decoder: Optional[Cropping2D] = None
        try:
            x = Reshape(reshaped_input)(input_encoded)
            x = Conv2D(16, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            if crop_up_sampling:
                x = Cropping2D(cropping=((1, 0), (0, 0)), data_format="channels_last")(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(64, (3, 3),  padding="same")(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)
            x = ZeroPadding2D(padding=(0, 3), data_format="channels_last")(x)
            decoder: Cropping2D = Cropping2D(cropping=((0, 0), (1, 0)),
                                             data_format="channels_last",
                                             name=decoder_layer_name)(x)
        except Exception as e:
            gv.logger.error(e)
        return decoder

    @staticmethod
    def build_autoencoder_system(input_shape_mel: tuple, input_shape_chroma: tuple,
                                 encoder_layer_name_mel: str, last_layer_name_mel: str,
                                 encoder_layer_name_chroma: str, last_layer_name_chroma: str,
                                 decoder_layer_name_mel: str, decoder_layer_name_chroma: str,
                                 model_name: str, mandatory_shape: int):
        autoencoder: Optional[Model] = None
        try:
            # Create inputs
            input_mel: Input = Input(input_shape_mel)
            input_chroma: Input = Input(input_shape_chroma)

            # Create encoder branches
            mel_encoder: Flatten = __class__.build_encoder_branch(inputs=input_mel,
                                                                  encoder_layer_name=encoder_layer_name_mel,
                                                                  last_layer_name=last_layer_name_mel,
                                                                  mandatory_shape=mandatory_shape)
            chroma_encoder: Flatten = __class__.build_encoder_branch(inputs=input_chroma,
                                                                     encoder_layer_name=encoder_layer_name_chroma,
                                                                     last_layer_name=last_layer_name_chroma,
                                                                     mandatory_shape=mandatory_shape)
            # Retrieve last layer output
            mel_encoder_shape = __class__.extract_output_shape_from_encoder(input_data=input_mel,
                                                                            encoder_output=mel_encoder,
                                                                            last_layer_name=last_layer_name_mel)
            chroma_encoder_shape = __class__.extract_output_shape_from_encoder(input_data=input_chroma,
                                                                               encoder_output=chroma_encoder,
                                                                               last_layer_name=last_layer_name_chroma)

            # Create decoder branches
            mel_decoder = __class__.build_decoder_branch(input_encoded=mel_encoder,
                                                         decoder_layer_name=decoder_layer_name_mel,
                                                         reshaped_input=mel_encoder_shape)
            chroma_decoder = __class__.build_decoder_branch(input_encoded=chroma_encoder,
                                                            decoder_layer_name=decoder_layer_name_chroma,
                                                            reshaped_input=chroma_encoder_shape,
                                                            crop_up_sampling=True)

            # Build autoencoder
            autoencoder = Model(inputs=[input_mel, input_chroma],
                                outputs=[mel_decoder, chroma_decoder],
                                name=model_name)
            # Show summary
            autoencoder.summary()
        except Exception as e:
            gv.logger.error(e)
        return autoencoder

    @staticmethod
    def extract_output_shape_from_encoder(input_data: Input, encoder_output: Flatten,
                                          last_layer_name: str):
        encoder_output_shape: tuple = ()
        try:
            encoder_model: Model = Model(input_data, encoder_output)
            encoder_output_shape: tuple = encoder_model.get_layer(last_layer_name).output_shape[1:]
        except Exception as e:
            gv.logger.error(e)
        return encoder_output_shape

    @staticmethod
    def select_optimizer(opt_name="adam"):
        optimizer: Optional[optimizers] = None
        try:
            if opt_name == 'sgd':
                optimizer = optimizers.SGD()
            elif opt_name == 'adagrad':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adadelta':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adam':
                optimizer = optimizers.Adam()
            elif opt_name == 'nadam':
                optimizer = optimizers.Nadam()
            else:
                gv.logger.warning('Not valid optimizer!. Please check the configuration script.')
        except Exception as e:
            gv.logger.error(e)
        return optimizer

    def compile_autoencoder(self, autoencoder: Model):
        try:
            optimizer: optimizers = self.select_optimizer(opt_name=self.optimizer_name)
            autoencoder.compile(optimizer=optimizer,
                                loss=self.losses,
                                metrics=self.metrics)
        except Exception as e:
            gv.logger.error(e)
        return autoencoder

    def train_autoencoder_generator(self, autoencoder: Model, mongo_manager: MongoDBManager,
                                    collection_name: str,
                                    train_ids: list, dev_ids: list):
        autoencoder_history: Optional[History] = None
        try:
            steps_per_epoch = int(len(train_ids) / self.batch_size)
            validation_steps = int(len(dev_ids) / self.batch_size)

            training_generator = DataGenerator(mongo_manager=mongo_manager,
                                               docs_uuids=train_ids, collection_name=collection_name,
                                               batch_size=self.batch_size, dim_mel=self.mel_dim,
                                               dim_chroma=self.chroma_dim,
                                               n_channels=self.n_channels, shuffle=True, to_fit=True)
            validation_generator = DataGenerator(mongo_manager=mongo_manager,
                                                 docs_uuids=dev_ids, collection_name=collection_name,
                                                 batch_size=self.batch_size, dim_mel=self.mel_dim,
                                                 dim_chroma=self.chroma_dim,
                                                 n_channels=self.n_channels, shuffle=True, to_fit=True)

            autoencoder_history: History = autoencoder.fit_generator(training_generator,
                                                                     validation_data=validation_generator,
                                                                     steps_per_epoch=steps_per_epoch,
                                                                     validation_steps=validation_steps,
                                                                     epochs=self.epochs)
        except Exception as e:
            gv.logger.error(e)

        return autoencoder_history

    @staticmethod
    def save_history(history: History, history_directory: str, history_name: str):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=history_directory)
            history_path = os.path.join(history_directory, history_name)
            # Write JSON
            write_json_file(data=history.history, filename=history_path)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def load_history(history_directory: str, history_name: str):
        model_history: Optional[History] = None
        try:
            history_path: str = os.path.join(history_directory, history_name)
            # Load model history
            model_history: History = read_json_file(history_path)
        except Exception as e:
            gv.logger.error(e)
        return model_history

    @staticmethod
    def load_trained_model(model_directory: str, model_name: str):
        model: Optional[Model] = None
        try:
            model_path: str = os.path.join(model_directory, model_name)
            # Load model
            model: Model = load_model(model_path)
        except Exception as e:
            gv.logger.error(e)
        return model

    @staticmethod
    def save_trained_model(model: Model, model_directory: str, model_name: str, extension: str = ".h5"):
        try:
            # Check if directory exists
            prepare_directory(dir_path_to_check=model_directory)
            # Check if extension is not included in the name of the model
            if len(model_name.split(".")) <= 1:
                model_name += extension
            # Merge path
            model_path: str = os.path.join(model_directory, model_name)
            # Save model
            model.save(model_path)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def load_encoder_model(model_directory: str, model_name: str, encoder_mel: str,
                           encoder_chroma: str):
        encoder: Optional[Model] = None
        try:
            model_path = os.path.join(model_directory, model_name)
            # Load model
            model = load_model(model_path)
            encoder: Model = Model(inputs=model.inputs,
                                   outputs=[model.get_layer(encoder_mel).output,
                                            model.get_layer(encoder_chroma).output])
        except Exception as e:
            gv.logger.error(e)
        return encoder

    @staticmethod
    def get_encoder_size_by_name(model: Model, layer_name: str):
        encoder_size: int = -1
        try:
            encoder_size: int = int(model.get_layer(layer_name).output_shape[1])
        except Exception as e:
            gv.logger.error(e)
        return encoder_size

    @staticmethod
    def generate_embeddings(model: Model, mel_data: list, chroma_data: list,
                            mel_dim: tuple, chroma_dim: tuple):

        x_all_embedding: np.array = np.array([])
        try:
            x_mel = np.array(mel_data).reshape(mel_dim)
            x_chroma = np.array(chroma_data).reshape(chroma_dim)

            x_embedding: list = model.predict([x_mel, x_chroma])
            x_all_embedding: np.array = np.concatenate([i for i in x_embedding], axis=1)
        except Exception as e:
            gv.logger.error(e)
        return x_all_embedding
