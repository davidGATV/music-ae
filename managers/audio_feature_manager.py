from helper.utils import download_file
from helper import global_variables as gv
from analysis.audio_analysis import AudioAnalysis
from managers.serializable_iterator import SerializableGenerator
import os, pickle
from bson.binary import Binary



class AudioFeatureManager:
    def __init__(self, parent_path: str, file_extension: str = ".mp3"):
        self.parent_path: str = parent_path
        self.file_extension: str = file_extension

    @staticmethod
    def download_audio_file(preview_url: str, parent_path: str, audio_name: str):
        done: bool = False
        try:
            # Download and store audioFile
            # audio_name = 'track' + '_' + str(uuid) + '.mp3'
            done: bool = download_file(url=preview_url, audio_path=parent_path, audio_name=audio_name)
        except Exception as e:
            gv.logger.error(e)
        return done

    @staticmethod
    def retrieve_audio_features(audio_dir: str, audio_filename: str, window: bool = True,
                                seconds_per_analysis: int = 6):
        features: dict = {}
        try:
            # Extract features from audio signal
            audio_analysis_man: AudioAnalysis = AudioAnalysis(audio_dir=audio_dir, audio_filename=audio_filename)
            features: dict = audio_analysis_man.execute_analysis(window=window,
                                                                 seconds_per_analysis=seconds_per_analysis)
        except Exception as e:
            gv.logger.error(e)
        return features

    def collect_audio_features_from_directory(self):
        try:
            tracks: iter = iter([os.path.join(self.parent_path, f) for f
                                 in os.listdir(self.parent_path) if
                                 f.endswith(self.file_extension)])

            for track in tracks:
                try:
                    audio_name: str = str(track.split(os.sep)[-1])
                    # Extract Features
                    data_features: dict = self.__class__.retrieve_audio_features(
                        audio_dir=self.parent_path,
                        audio_filename=audio_name)
                    yield data_features
                except Exception as e:
                    gv.logger.warning(e)
                    continue
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def build_documents(input_document: dict, uuid_param: str):
        output_document: SerializableGenerator = SerializableGenerator([])
        try:
            # Generate UUID
            # doc_embedding: Binary = Binary(pickle.dumps(doc_embedding_np, protocol=2))
            output = [{'mel': Binary(pickle.dumps(mel, protocol=2)),
                       'chroma': Binary(pickle.dumps(chroma, protocol=2)),
                       'mel_size': input_document["mel_size"],
                       'chroma_size': input_document["chroma_size"],
                       'total_segments': input_document["n_segments"],
                       'segment_id': i,
                       'track_id': uuid_param} for i, mel, chroma in
                      zip(list(range(1, input_document["n_segments"] + 1)),
                          list(input_document["mel"]),
                          list(input_document["chroma"]))]
            # Generate documents
            output_document: SerializableGenerator = SerializableGenerator(output)
        except Exception as e:
            gv.logger.error(e)
        return output_document