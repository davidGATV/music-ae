# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 13:51:19 2019

@author: dmarg
"""

import librosa as li
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import os
from pydub.utils import mediainfo
from helper import global_variables as gv
from sklearn.preprocessing import minmax_scale


class AudioAnalysis:
    def __init__(self, audio_dir: str, audio_filename: str):
        self.audio_dir: str = audio_dir
        self.audio_filename: str = audio_filename
        self.file_path: str = os.path.join(self.audio_dir, self.audio_filename)

        # Audio Parameters
        self.mono: bool = True
        self.sr: int = gv.sr
        self.hop_length: int = gv.hop_length
        self.n_fft: int = gv.n_fft
        self.n_mels: int = gv.n_mels
        self.n_chroma: int = gv.n_chroma
        self.n_octaves: int = gv.n_octaves
        self.fmin: int = gv.fmin
        self.fmax: int = gv.fmax
        self.power: float = gv.power
        self.roll_percent_min: float = gv.roll_percent_min
        self.roll_percent_max: float = gv.roll_percent_max
        self.roll_percent: float = gv.roll_percent

    def execute_analysis(self, window: bool = True, seconds_per_analysis: int = 5):
        features: dict = {"mel": iter([]), "chroma": iter([]), "mel_size": -1,
                          "chroma_size": -1, "n_segments": -1}
        try:
            # Load audioFile
            y, sr = self.load_audio(file_path=self.file_path, sr=self.sr, mono=self.mono)
            mel_data: list = []
            chroma_data: list = []
            segment_id: int = 1
            if window:
                n_samples: int = int(seconds_per_analysis * self.sr)
                done: bool = True
                offset: int = 0
                features_segment: dict = {}
                while done:
                    if offset + n_samples > len(y):
                        done = False
                    else:
                        y_analysis: np.array = y[offset: offset + n_samples]
                        offset += n_samples
                        # Process features
                        features_segment: dict = self.compute_naive_audio_features(y=y_analysis)
                        # Add segment id to dictionary key
                        mel_data.append(features_segment["mel"].flatten().tolist())
                        chroma_data.append(features_segment["chroma"].flatten().tolist())
                        segment_id += 1
            else:
                # Process features
                features_segment: dict = self.compute_naive_audio_features(y=y)
                mel_data.append(features_segment["mel"].flatten().tolist())
                chroma_data.append(features_segment["chroma"].flatten().tolist())
            # Add Original sizes
            features["mel"] = iter(mel_data)
            features["chroma"] = iter(chroma_data)
            features["mel_size"] = features_segment["mel_size"]
            features["chroma_size"] = features_segment["chroma_size"]
            features["n_segments"] = segment_id - 1
        except Exception as e:
            gv.logger.error(e)
        return features

    def compute_naive_audio_features(self, y: np.array):
        features: dict = {"mel": None, "chroma": None, "mel_size": None, "chroma_size": None}
        try:
            feature_length: int = self.extract_feature_length_from_file(y, sr=self.sr, n_fft=self.n_fft,
                                                                        hop_length=self.hop_length)
            # Compute Mel spectrogram
            mel = __class__.compute_mel_spectrogram(y, sr=self.sr, feature_length=feature_length,
                                                    n_mels=self.n_mels, power=self.power,
                                                    n_fft=self.n_fft, hop_length=self.hop_length,
                                                    fmin=self.fmin, fmax=self.fmax)
            features["mel"] = __class__.normalize_mel_spectrogram(s=mel, power=self.power)
            features["mel_size"] = (self.n_mels, feature_length)

            # Compute Chromagram-cqt
            features["chroma"] = __class__.compute_chromagram_cqt(y=y, sr=self.sr, feature_length=feature_length,
                                                                  n_chroma=self.n_chroma, n_octaves=self.n_octaves,
                                                                  hop_length=self.hop_length)
            features["chroma_size"] = (self.n_chroma, feature_length)
        except Exception as e:
            gv.logger.error(e)
        return features

    def extract_feature_length_from_file(self, y: np.array, sr: int, n_fft: int, hop_length: int):
        feature_length: int = -1
        try:
            seconds: int = self.__class__.get_duration(y=y, sr=sr, n_fft=n_fft, hop_length=hop_length)
            feature_length: int = self.__class__.get_feature_length(seconds=seconds,
                                                                    sr=sr, hop_length=hop_length)
        except Exception as e:
            gv.logger.error(e)
        return feature_length

    @staticmethod
    def get_sample_rate(file_path: str):
        sr: int = 44100
        try:
            audio_info: dict = mediainfo(file_path)
            sr: int = int(audio_info['sample_rate'])
        except Exception as e:
            gv.logger.error(e)
        return sr

    @staticmethod
    def load_audio(file_path: str, sr: int = None, mono: bool = True):
        y: np.array = None
        try:
            y, sr = li.load(file_path, sr=sr, mono=mono)
        except Exception as e:
            gv.logger.error(e)
        return y, sr

    @staticmethod
    def get_duration(y: np.array, sr: int, n_fft: int, hop_length: int):
        seconds: int = -1
        try:
            seconds: int = li.core.get_duration(y, sr, n_fft=n_fft, hop_length=hop_length)
        except Exception as e:
            gv.logger.error(e)
        return seconds

    @staticmethod
    def normalize_mel_spectrogram(s: np.array, power: float):
        s_db: np.array = np.array([])
        try:
            if power == 2.0:
                s_db = li.power_to_db(s, ref=np.max)
            elif power == 1.0:
                s_db = li.amplitude_to_db(s, ref=np.max)
            else:
                # Per-Channel Energy normalization
                s_db = librosa.pcen(s)
        except Exception as e:
            gv.logger.error(e)
        return s_db

    @staticmethod
    def get_feature_length(seconds: int, sr: int, hop_length: int):
        feature_length: int = -1
        try:
            feature_length: int = int(np.ceil((seconds*sr) / hop_length))
        except Exception as e:
            gv.logger.error(e)
        return feature_length

    @ staticmethod
    def compute_mfcc(y: np.array, sr: int, feature_length: int, n_mels: int = 64,
                     n_fft: int = 2048, hop_length: int = 512):
        # Init value
        mfcc = -99 * np.ones((n_mels, feature_length))
        try:
            mfcc = li.feature.mfcc(y=y, sr=sr, n_mels=n_mels, n_fft=n_fft, hop_length=hop_length)
        except Exception as e:
            gv.logger.error(e)
        return mfcc

    @staticmethod
    def compute_chromagram_cqt(y: np.array, sr: int, feature_length: int, n_chroma: int = 12,
                               n_octaves: int = 7, hop_length: int = 512):
        # Init value
        chroma_cqt = -99 * np.ones((n_chroma, feature_length))
        try:
            chroma_cqt = li.feature.chroma_cqt(y=y, sr=sr, hop_length=hop_length, n_chroma=n_chroma,
                                               n_octaves=n_octaves)
        except Exception as e:
            gv.logger.error(e)
        return chroma_cqt

    @staticmethod
    def compute_mel_spectrogram(y: np.array, sr: int, feature_length: int, n_mels: int = 64,
                                power: float = 2.0, n_fft: int = 2048, hop_length: int = 512,
                                fmin: int = 20, fmax: int = 8000):
        # Init value
        mel: np.array = -99 * np.ones((n_mels, feature_length))
        try:
            mel: np.array = li.feature.melspectrogram(y=y, sr=sr, n_fft=n_fft, hop_length=hop_length,
                                                      power=power, n_mels=n_mels, fmin=fmin, fmax=fmax)
        except Exception as e:
            gv.logger.error(e)
        return mel

    @staticmethod
    def compute_zero_crossing_rate(y: np.array, feature_length: int, frame_length: int = 2048,
                                   hop_length: int = 512):
        # Init value
        zcr: np.array = -99 * np.ones((1, feature_length))
        try:
            zcr: np.array = li.feature.zero_crossing_rate(y=y, frame_length=frame_length,
                                                          hop_length=hop_length,
                                                          center=True)
        except Exception as e:
            gv.logger.error(e)

        return zcr

    @staticmethod
    def compute_tonnetz(y: np.array, sr: int, feature_length: int, chroma_cqt: np.array = None):
        # Init value
        tonnetz: np.array = -99 * np.ones((6, feature_length))
        try:
            tonnetz: np.array = li.feature.tonnetz(y=y, sr=sr, chroma=chroma_cqt)
        except Exception as e:
            gv.logger.error(e)
        return tonnetz

    @staticmethod
    def plot_mel_spectrogram(s: np.array, sr: int, fmin: int, fmax: int, hop_length: int):
        try:
            plt.figure()
            librosa.display.specshow(s, x_axis='time',
                                     y_axis='mel', sr=sr,
                                     fmax=fmax, fmin=fmin, hop_length=hop_length)

            plt.colorbar(format='%+2.0f dB')
            plt.title('Mel-frequency spectrogram')
            plt.tight_layout()
            plt.show()
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def plot_chroma_cqt_spectrogram(s: np.array, sr: int, hop_length: int):
        try:
            plt.figure()
            librosa.display.specshow(s, x_axis='time',
                                     y_axis='chroma', sr=sr, hop_length=hop_length)
            plt.colorbar()
            plt.title('Chromagram CQT')
            plt.tight_layout()
            plt.show()
        except Exception as e:
            gv.logger.error(e)