import numpy as np
import ast, hashlib, os, subprocess, json, operator
from pandas.io.json import json_normalize
from helper import global_variables as gv


def generate_uuid_from_string(data_uuid: list):
    uuid: str = ""
    try:
        # Concatenate elements from list
        uuid_data: str = "".join(data_uuid)
        hash_object = hashlib.sha512(uuid_data.encode('utf-8'))
        uuid: str = hash_object.hexdigest()
    except Exception as e:
        gv.logger.error(e)
    return uuid


def download_file(url: str, audio_path: str, audio_name: str):
    done: bool = False
    try:
        audio_path = os.path.join(audio_path, audio_name)
        if url is not None and audio_path is not None:
            gv.logger.info('Downloading and storing File: %s', audio_name)
            request = 'Curl ' + url + ' -o ' + audio_path
            subprocess.call(request)
            done = True
    except Exception as e:
        gv.logger.error(e)
    return done


def prepare_directory(dir_path_to_check: str):
    res: bool = False
    try:
        if not os.path.exists(dir_path_to_check):
            os.makedirs(dir_path_to_check)
        res = True
    except Exception as e:
        gv.logger.error(e)
    return res


def update_filter_kernel_size(filter_size, kernel_size, increase_filter=None, increase_kernel=None, step=1, kernel_step=(2, 2)):
    try:
        if increase_filter:
            filter_size = int(filter_size * (2 ** step))
        elif increase_filter is None:
            pass
        else:
            filter_size = int(filter_size / (2 ** step))

        if increase_kernel:
            kernel_size = tuple(map(operator.add, kernel_size, kernel_step))
        elif increase_kernel is None:
            pass
        else:
            kernel_size = tuple(map(operator.sub, kernel_size, kernel_step))
    except Exception as e:
        gv.logger.error(e)
    return filter_size, kernel_size


def stack_features(data_df, feature_size, col_name, n_cols, new_axis=True):
    stacked_data_np = None
    try:
        cols = [col_name + "_" + str(i+1) for i in range(n_cols)]

        # Stack data into a single DataFrame
        stacked_data_df = data_df[cols].stack().values.tolist()

        # Convert list of str to list of numpy
        if isinstance(stacked_data_df[0],str):
            x = []
            for i in stacked_data_df:
                x.append(ast.literal_eval(i))
        else:
            x = stacked_data_df

        # Convert to Numpy and reshape
        stacked_data_np = np.array(x).reshape((-1, feature_size[0], feature_size[1]))

        # Add new axis
        if new_axis:
            stacked_data_np = stacked_data_np[:, :, :, np.newaxis]

    except Exception as e:
        gv.logger.error(e)
    return stacked_data_np


def generate_train_test_set_numpy(data_np, train_split=.8):
    x_train, x_test = None, None
    try:
        t = int(train_split*data_np.shape[0])
        x_train, x_test = data_np[:t, :], data_np[t:, :]
    except Exception as e:
        gv.logger.error(e)
    return x_train, x_test


def write_json_file(data: dict, filename: str):
    try:
        json_data = json.dumps(str(data))
        with open(filename, 'w') as file:
            json.dump(json_data, file)
    except Exception as e:
        gv.logger.error(e)


def read_json_file(filename):
    data = None
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
    except Exception as e:
        gv.logger.error(e)
    return data


def adequate_shape(x, add_axis=True):
    x_resized = np.array([])
    try:
        # New dimension
        n_dim = int(np.sqrt(x.shape[1]*x.shape[2]*x.shape[3]))

        # Reshape
        x_reshaped = x.reshape((x.shape[0], x.shape[1]*x.shape[2]*x.shape[3]))

        # Resize numpy using the new dimension
        for i in range(x_reshaped.shape[0]):
            x_res = np.resize(x[i], (n_dim, n_dim))
            x_res = x_res[np.newaxis, :, :]
            if i == 0:
                x_resized = x_res
            else:
                x_resized = np.concatenate([x_resized, x_res], axis=0)
        if add_axis:
            x_resized = x_resized[:, :, :, np.newaxis]
    except Exception as e:
        gv.logger.error(e)
    return x_resized


def get_classification_thresholds(data, n):
    thresholds = []
    try:
        all_perc = list(np.linspace(10, 90, n-1))
        for p in all_perc:
            percentile = np.round(np.percentile(data, p),3)
            thresholds.append(percentile)
    except Exception as e:
        gv.logger.error(e)
    return thresholds


def check_range(a,b, x):
    res = False
    try:
        if a <= x < b:
            res = True
    except Exception as e:
        gv.logger.error(e)
    return res


def get_classification_outputs(x, thresholds, min_x=0, max_x=100):
    y = None
    done = False
    try:
        for i, th in enumerate(thresholds):
            # First threshold
            if i == 0:
               a = min_x
               b = th
            # Other case
            else:
                a = thresholds[i-1]
                b = thresholds[i]
            # Check range
            res = check_range(a, b, x)
            if res:
                y = i+1
                done = True
                break
        if not done:
            a = thresholds[-1]
            b = max_x
            res = check_range(a=a, b=b, x=x)
            if res:
                y = len(thresholds) + 1
            else:
                y = -1
    except Exception as e:
        gv.logger.error(e)
    return y


def json_data_to_dataframe(data):
    df = None
    try:
        # Convert to json
        if not isinstance(data, str):
            data = json.dumps(data)
        json_data = json.loads(data)
        # Convert to DataFrame
        df = json_normalize(json_data)
    except Exception as e:
        gv.logger.error(e)
    return df


def get_region(country_code):
    region = "unknown"
    try:
        if country_code in gv.europe:
            region = "EU"
        elif country_code in gv.north_america:
            region = "NA"
        elif country_code in gv.south_america:
            region = "SA"
        elif country_code in gv.africa:
            region = "AF"
        elif country_code in gv.asia:
            region = "AS"
        elif country_code in gv.oceania:
            region = "OC"
    except Exception as e:
        gv.logger.error(e)
    return region


def get_json_object(data):
    json_data = None
    try:
        json_data = json.loads(data)
    except Exception as e:
        gv.logger.error(e)
    return json_data
