import numpy as np
from helper import global_variables as gv
from tensorflow import keras
from managers.mongodb_manager import MongoDBManager
from helper.nd_standard_scaler import NDStandardScaler


class DataGenerator(keras.utils.Sequence):
    """Generates data for Keras
    Sequence based data generator. Suitable for building data generator for training and prediction.
    """
    def __init__(self, mongo_manager: MongoDBManager, docs_uuids: list, collection_name: str,
                 batch_size: int, dim_mel: tuple, dim_chroma: tuple, n_channels: int = 1,
                 shuffle: bool = True, to_fit: bool = True):

        self.mongo_manager: MongoDBManager = mongo_manager
        self.docs_uuids: list = docs_uuids
        self.collection_name: str = collection_name
        self.batch_size: int = batch_size
        self.dim_mel: tuple = dim_mel
        self.dim_chroma: tuple = dim_chroma
        self.n_channels = n_channels
        self.shuffle = shuffle
        self.to_fit: bool = to_fit
        self.indexes: np.arange = np.arange(len(self.docs_uuids))
        self.scaler: NDStandardScaler = NDStandardScaler()
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int(np.floor(len(self.docs_uuids) / self.batch_size))

    def __getitem__(self, index):
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        docs_ids_temp: list = [self.docs_uuids[k] for k in indexes]

        # Generate data
        x: list = self.generate_x(docs_ids_temp)
        if self.to_fit:
            y = self.generate_x(docs_ids_temp)
            return x, y
        else:
            return x

    def on_epoch_end(self):
        """Updates indexes after each epoch
        """
        self.indexes: np.arange = np.arange(len(self.docs_uuids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def generate_x(self, doc_ids: list):
        # Initialization
        x_mel: np.array = np.empty((self.batch_size, *self.dim_mel, self.n_channels))
        x_chroma: np.array = np.empty((self.batch_size, *self.dim_chroma, self.n_channels))

        # Generate data
        for i, doc_id in enumerate(doc_ids):
            # Store sample
            x_mel[i, ], x_chroma[i, ] = self.load_data_from_mongodb(collection_name=self.collection_name,
                                                                    doc_id=doc_id)
        # Scale data
        x_mel = self.scaler.fit_transform(x_mel)
        x_chroma = self.scaler.fit_transform(x_chroma)
        x: list = [x_mel, x_chroma]
        return x

    def load_data_from_mongodb(self, collection_name: str, doc_id: str):
        x_mel: np.array = np.empty((self.batch_size, *self.dim_mel, self.n_channels))
        x_chroma: np.array = np.empty((self.batch_size, *self.dim_chroma, self.n_channels))

        try:
            doc_data: dict = self.mongo_manager.get_document_by_id(collection_name=collection_name,
                                                                   uuid=doc_id)
            # Process Mel
            x_mel: np.array = np.array(doc_data["mel"]).reshape(self.dim_mel)
            # Expand dimension
            x_mel = np.expand_dims(x_mel, axis=len(self.dim_mel))

            # print(x_mel.mean())
            # print(x_mel.std())
            # print(np.min(x_mel))
            # print(np.max(x_mel))

            # Check if all zeros
            sparsity_level = 1 - (np.count_nonzero(x_mel.flatten()) / x_mel.flatten().shape[0])
            if sparsity_level >= .9:
                gv.logger.warning("Sparse signal ...")
            # Process Chroma
            x_chroma: np.array = np.array(doc_data["chroma"]).reshape(self.dim_chroma)
            # Expand dimension
            x_chroma = np.expand_dims(x_chroma, axis=len(self.dim_chroma))

        except Exception as e:
            gv.logger.error(e)
        return x_mel, x_chroma
