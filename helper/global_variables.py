import os
from helper.custom_log import init_logger
import warnings
warnings.filterwarnings('ignore')

# =================================================
#  -------------- GLOBAL PARAMETERS ---------------
# =================================================

global_parent_dir = os.getenv("GLOBAL_PARENT_DIR") if "GLOBAL_PARENT_DIR" in os.environ else ""
os.environ['app_logs'] = os.path.join(global_parent_dir, "app_logs")
logger = init_logger(__name__, testing_mode=False)

audio_dataset_dir: str = "Dataset_test"
uuid_col: str = "uuid"
file_extension: str = ".mp3"
# =================================================
#  --------------- BBDD PARAMETERS ----------------
# =================================================
db_host: str = os.getenv("MONGODB_HOST") if "MONGODB_HOST" in os.environ else "localhost"
db_port: str = os.getenv("MONGODB_PORT") if "MONGODB_PORT" in os.environ else "27017"
db_username: str = os.getenv("MONGODB_USERNAME") if "MONGODB_USERNAME" in os.environ else ""
db_password: str = os.getenv("MONGODB_PASSWORD") if "MONGODB_PASSWORD" in os.environ else ""
audio_db_name: str = os.getenv("MONGODB_NAME") if "MONGODB_NAME" in os.environ else "spotify"
audio_collection_name: str = os.getenv("AUDIO_COLLECTION_NAME") if "AUDIO_COLLECTION_NAME" in\
                                                          os.environ else "audio_features_pcen"
embedding_collection_name: str = os.getenv("EMBEDDING_COLLECTION_NAME") if "EMBEDDING_COLLECTION_NAME" in\
                                                          os.environ else "audio_embeddings_pcen"

# =================================================
#  -------------- AUDIO PARAMETERS ----------------
# =================================================

sr: int = 44100
n_mels: int = 2**5
hop_length: int = 512
n_fft: int = 2048
fmin: int = 50
fmax: int = int(sr/2)
seconds_per_analysis: int = 6
roll_percent_min: float = 0.1
roll_percent_max: float = 0.85
roll_percent: float = 0.5
power: float = 2.0
n_chroma: int = 12
n_octaves: int = 7


#
# =================================================
#  -------------- AE PARAMETERS ----------------
# =================================================
test_size: float = 0.2
dev_size: float = 0.3
mandatory_shape: int = 512
encoder_layer_name_mel: str = "encoder_mel"
encoder_layer_name_chroma: str = "encoder_chroma"
last_layer_name_mel: str = "last_processed_layer_mel"
last_layer_name_chroma: str = "last_processed_layer_chroma"
decoder_layer_name_mel: str = "decoder_mel"
decoder_layer_name_chroma: str = "decoder_chroma"
model_name: str = os.getenv("AE_NAME") if "AE_NAME" in os.environ else "MUSIC-AENET"
metrics: list = ["mae"]
losses: list = ["mse", "mse"]
optimizer_name: str = "adam"
batch_size: int = 64
epochs: int = 1
n_channels: int = 1