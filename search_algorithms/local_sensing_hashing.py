import numpy as np


class HashTable:
    def __init__(self, hash_size: int, inp_dimensions: int):
        self.hash_size = hash_size
        self.inp_dimensions = inp_dimensions
        self.hash_table = dict()
        self.projections = np.random.randn(self.hash_size, inp_dimensions)

    def generate_hash(self, inp_vector: np.ndarray):
        res_sign: np.ndarray = (np.dot(inp_vector, self.projections.T) > 0).astype('int')
        return ''.join(res_sign.astype('str'))

    def __setitem__(self, inp_vec: np.ndarray, label: str):
        hash_value: str = self.generate_hash(inp_vec)
        self.hash_table[hash_value] = self.hash_table \
                                          .get(hash_value, list()) + [label]

    def __getitem__(self, inp_vec: np.ndarray):
        hash_value = self.generate_hash(inp_vec)
        return self.hash_table.get(hash_value, [])


class LSH:
    def __init__(self, num_tables: int, hash_size: int, inp_dimensions: int):
        self.num_tables: int = num_tables
        self.hash_size: int = hash_size
        self.inp_dimensions: int = inp_dimensions
        self.hash_tables: list = list()
        for i in range(self.num_tables):
            self.hash_tables.append(HashTable(self.hash_size,
                                              self.inp_dimensions))

    def __setitem__(self, inp_vec: np.ndarray, label: str):
        for table in self.hash_tables:
            table[inp_vec] = label

    def __getitem__(self, inp_vec: np.ndarray):
        results: list = list()
        for table in self.hash_tables:
            results.extend(table[inp_vec])
        return list(set(results))